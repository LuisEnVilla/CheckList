var express = require('express');
var qr = require('qr-image');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET home page. */
router.get('/app/:number', function(req, res, next) {
	db.checklist.update({"phone":req.params.number},{$inc:{"check":1}},function (err,data) {
		if (err) throw err;
  		res.status(200).jsonp({
				"ack": "TRUE"});
	});
});

/* GET home page. */
router.get('/registro', function(req, res, next) {
  res.render('registroMaestro');
});

/* GET home page. */
router.post('/registro', function(req, res, next) {
	var registro = new db.maestro({
		nombre: req.body.name,
		email: req.body.email,
		pw : req.body.pw,
		clase: req.body.clase
	})
	registro.save();
  res.render('pago');
});

router.get('/registroAlumno/:email', function(req, res, next) {
	db.maestro.findOne({"email":req.params.email},function (err,data) {
		if (err) throw err;
  		res.render('registroAlumno',{maestro:data});
	});
});

router.post('/registroAlumno/:email', function(req, res, next) {
	var registro = new db.checklist({
		phone: req.body.phone,
		nombre: req.body.name,
		check:0
	})
	registro.save();
	var code = qr.image(registro.phone+registro.nombre, { type: 'png'});
  	res.type('png');
  	code.pipe(res);
	/*db.maestro.findOne({"email":req.params.email},function (err,data) {
		if (err) throw err;
  		res.render('registroAlumno',{maestro:data});
	});*/
});

router.get('/dashboard/:email', function(req, res, next) {
	db.maestro.findOne({"email":req.params.email},function (err,data) {
		if (err) throw err;
		db.checklist.find({},function (err,data2) {
			if (err) throw err;
	  		res.render('dash',{maestro:data,alumnos:data2});
	  	});
	});
});

router.get('/reporte/:email', function(req, res, next) {
	db.maestro.findOne({"email":req.params.email},function (err,data) {
		if (err) throw err;
		db.checklist.find({},function (err,data2) {
			if (err) throw err;
			//var json = JSON.parse(data2);
        	res.csv(data2, "reporte" + data.nombre + ".csv", ["nombre", "check"]);
	  	});
	});
});

module.exports = router;
