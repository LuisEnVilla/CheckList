var mongoose = require('mongoose');
var mongodb_connection_string = 'mongodb://woo:DJ4mixDJ4mix@ds043062.mongolab.com:43062/woodb';

mongoose.connect(mongodb_connection_string,function(err, res) {
	if(!err) console.log('Conectado a BD Cheker');
});

global.db = {
	mongoose: mongoose,
	checklist: require('./checklist')(mongoose),
	maestro: require('./maestro')(mongoose)
};

module.exports = global.db;