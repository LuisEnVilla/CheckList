module.exports = function (mongoose) {
	var Schema = mongoose.Schema;
	// Objeto modelo de Mongoose
	var maestro = new Schema({
		// Propiedades
		nombre: String,
		email: String,
		pw : String,
		clase: String
	});
	maestro.set('collection', 'maestro');
	return mongoose.model('maestro', maestro);
}