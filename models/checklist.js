module.exports = function (mongoose) {
	var Schema = mongoose.Schema;
	// Objeto modelo de Mongoose
	var checklist = new Schema({
		// Propiedades
		phone: String,
		nombre: String,
		check: Number
	});
	checklist.set('collection', 'checklist');
	return mongoose.model('checklist', checklist);
}